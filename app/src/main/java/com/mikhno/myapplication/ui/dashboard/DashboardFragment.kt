package com.mikhno.myapplication.ui.dashboard

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.mikhno.myapplication.R
import com.mikhno.myapplication.utils.secondModelArlanga


class DashboardFragment : Fragment() {

    private lateinit var dashboardViewModel: DashboardViewModel

    @SuppressLint("ResourceType")
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        dashboardViewModel =
                ViewModelProviders.of(this).get(DashboardViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_dashboard, container, false)
        val button = root.findViewById<Button>(R.id.ssubmit)
        button.setOnClickListener {
            val lambda = root.findViewById<TextView>(R.id.slambda).text.toString().toDouble()
            val channels = root.findViewById<TextView>(R.id.schannels).text.toString().toInt()
            val service_time = root.findViewById<TextView>(R.id.sservice_time).text.toString().toDouble()
            val slength = root.findViewById<TextView>(R.id.slength).text.toString().toInt()
            val needP = root.findViewById<TextView>(R.id.sNeedP).text.toString().toDouble()
            val result = secondModelArlanga(channels, lambda, service_time, slength, needP)

            val tableLayout = root.findViewById<TableLayout>(R.id.stableLayout)
            tableLayout.removeAllViews()
            setRow(tableLayout, "p_0 = ${result.p0}")
            setRow(tableLayout, "Вероятность отказа = ${result.potkaz}")
            setRow(tableLayout, "Вероятность обслуживания = ${result.pobsl}")
            setRow(tableLayout, "Абсолютная пропускная способность СМО = ${result.abs_prop_spos}")
            setRow(tableLayout, "Среднее число занятый каналов = ${result.avg_zan_canal}")
            setRow(tableLayout, "Среднее число требований в очереди = ${result.avg_in_que}")
            setRow(tableLayout, "Среднее число требований в системе = ${result.avg_treb_in_system}")
            setRow(tableLayout, "Среднее время пребывания требования в очереди = ${result.w0}")
            setRow(tableLayout, "Среднее время пребывания требования в СМО = ${result.wc}")
            setRow(tableLayout, "Требуемая емкость помещения = ${result.m}")
        }
        return root
    }

    private fun setRow(tableLayout: TableLayout, text: String) {
        val row = TableRow(context)
        val lp =
            TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT)
        row.layoutParams = lp
        val tv = TextView(context)
        tv.text = text
        tv.textSize = 15.0F
        tv.maxWidth = 900
        tv.setPadding(0, 20, 0, 0)
        row.addView(tv)
        tableLayout.addView(row)
    }
}
