package com.mikhno.myapplication.ui.dashboard

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mikhno.myapplication.utils.firstModelArlanga

class DashboardViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = firstModelArlanga(2, 1.0, 2.0, 1).toString()
    }
    val text: LiveData<String> = _text
}