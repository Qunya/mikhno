package com.mikhno.myapplication.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.mikhno.myapplication.R
import com.mikhno.myapplication.utils.firstModelArlanga

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        val button = root.findViewById<Button>(R.id.fsubmit)
        button.setOnClickListener {
            val lambda = root.findViewById<TextView>(R.id.flambda).text.toString().toDouble()
            val channels = root.findViewById<TextView>(R.id.fchannels).text.toString().toInt()
            val service_time = root.findViewById<TextView>(R.id.fservice_time).text.toString().toDouble()
            val state_number = root.findViewById<TextView>(R.id.fstate_number).text.toString().toInt()
            val result = firstModelArlanga(channels, lambda, service_time, state_number)

            val tableLayout = root.findViewById<TableLayout>(R.id.ftableLayout)
            tableLayout.removeAllViews()
            setRow(tableLayout, "p_0 = ${result.p0}")
            setRow(tableLayout, "p_k = ${result.pk}")
            setRow(tableLayout, "Вероятность отказа = ${result.potkaz}")
            setRow(tableLayout, "Вероятность обслуживания = ${result.pobsl}")
            setRow(tableLayout, "Абсолютная пропускная способность СМО = ${result.abs_prop_spos}")
            setRow(tableLayout, "Среднее число занятый каналов = ${result.avg_zan_canal}")
        }
        return root
    }

    private fun setRow(tableLayout: TableLayout, text: String) {
        val row = TableRow(context)
        val lp =
            TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT)
        row.layoutParams = lp
        val tv = TextView(context)
        tv.text = text
        tv.textSize = 15.0F
        tv.maxWidth = 900
        tv.setPadding(0, 20, 0, 0)
        row.addView(tv)
        tableLayout.addView(row)
    }
}
