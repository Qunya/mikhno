package com.mikhno.myapplication.ui.notifications

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.mikhno.myapplication.R
import com.mikhno.myapplication.utils.firstModelArlanga
import com.mikhno.myapplication.utils.nChannelSMOInfinityQue


class NotificationsFragment : Fragment() {

    private lateinit var notificationsViewModel: NotificationsViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        notificationsViewModel =
                ViewModelProviders.of(this).get(NotificationsViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_notifications, container, false)
        val button = root.findViewById<Button>(R.id.nsubmit)
        button.setOnClickListener {
            val lambda = root.findViewById<TextView>(R.id.nlambda).text.toString().toDouble()
            val channels = root.findViewById<TextView>(R.id.nchannels).text.toString().toInt()
            val service_time = root.findViewById<TextView>(R.id.nservice_time).text.toString().toDouble()
            val result = nChannelSMOInfinityQue(channels, lambda, service_time)

            val tableLayout = root.findViewById<TableLayout>(R.id.ntableLayout)
            tableLayout.removeAllViews()
            setRow(tableLayout, "p_0 = ${result.p0}")
            setRow(tableLayout, "Вероятность отказа = ${result.potkaz}")
            setRow(tableLayout, "Вероятность обслуживания = ${result.pobsl}")
            setRow(tableLayout, "Абсолютная пропускная способность СМО = ${result.abs_prop_spos}")
            setRow(tableLayout, "Среднее число занятый каналов = ${result.avg_zan_canal}")
            setRow(tableLayout, "Средняя длина очереди = ${result.avg_length_que}")
            setRow(tableLayout, "Среднее число требований в системе = ${result.avg_count_treb}")
            setRow(tableLayout, "Среднее время пребывания требования в очереди = ${result.avg_time_wait}")
            setRow(tableLayout, "Среднее время пребывания требования в СМО = ${result.avg_time_in_SMO}")
            setRow(tableLayout, "Оптимальное число каналов для данной СМО = ${result.opt_canal}")
        }
        return root
    }

    private fun setRow(tableLayout: TableLayout, text: String) {
        val row = TableRow(context)
        val lp =
            TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT)
        row.layoutParams = lp
        val tv = TextView(context)
        tv.text = text
        tv.textSize = 15.0F
        tv.maxWidth = 900
        tv.setPadding(0, 20, 0, 0)
        row.addView(tv)
        tableLayout.addView(row)
    }


}
