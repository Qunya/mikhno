package com.mikhno.myapplication.utils

fun calculateFactorial(n: Int): Int {
    var result = 1
    for (i in 1..n) {
        result *= i
    }
    return result
}