package com.mikhno.myapplication.utils

import kotlin.math.pow

data class NChannelSMOInfinityQue(
    val p0: Double,
    val potkaz: Int = 0,
    val pobsl: Int = 1,
    val abs_prop_spos: Double,
    val avg_zan_canal: Double,
    val avg_length_que: Double,
    val avg_count_treb: Double,
    val avg_time_wait: Double,
    val avg_time_in_SMO: Double,
    val opt_canal: Int
)

fun nChannelSMOInfinityQue(n: Int, lyambda: Double, tObsl: Double): NChannelSMOInfinityQue {
    val p0 = nSth(n, lyambda, tObsl)
    val potkaz = 0
    val pobsl = 1
    val abs_prop_spos = lyambda * pobsl
    val avg_zan_canal = lyambda * tObsl * pobsl
    val avg_length_que = (n*(lyambda*tObsl).pow(n+1))/(calculateFactorial(n) * (n - lyambda*tObsl).pow(2))*p0
    val avg_count_treb = avg_length_que + avg_zan_canal
    val avg_time_wait = avg_length_que/abs_prop_spos
    val avg_time_in_SMO = avg_count_treb/abs_prop_spos
    val opt_canal = search_n_opt(lyambda, tObsl, p0)
    return NChannelSMOInfinityQue(p0, potkaz, pobsl, abs_prop_spos, avg_zan_canal, avg_length_que, avg_count_treb, avg_time_wait, avg_time_in_SMO, opt_canal)
}

fun nSth(n: Int, lyambda: Double, tObsl: Double): Double {
    var rez = 0.0
    for (k in 0..n + 1) {
        rez += ((lyambda * tObsl).pow(k.toDouble())) / calculateFactorial(k)
    }
    rez += ((lyambda * tObsl).pow((n + 1).toDouble())) / (calculateFactorial(n) * (n - lyambda * tObsl))
    return rez.pow(-1)
}

fun func_izd(n: Int, lyambda: Double, tObsl: Double, p0: Double): Double {
    return (n * (lyambda * tObsl).pow(n + 1)) / (calculateFactorial(n) * (n - lyambda * tObsl).pow(2)) * p0 + n - lyambda * tObsl
}

fun search_n_opt(lyambda: Double, tObsl: Double, p0: Double): Int {
    var i = (lyambda * tObsl + 1).toInt()
    while (func_izd(i, lyambda, tObsl, p0) > func_izd(i + 1, lyambda, tObsl, p0)) {
        i += 1
    }
    return i
}