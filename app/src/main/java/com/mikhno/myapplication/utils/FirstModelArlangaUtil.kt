package com.mikhno.myapplication.utils

import kotlin.math.pow

data class FirstModelArlanga(
    val p0: Double,
    val pk: Double,
    val potkaz: Double,
    val pobsl: Double,
    val abs_prop_spos: Double,
    val avg_zan_canal: Double
)

fun firstModelArlanga(n: Int, lyambda: Double, tObsl: Double, k: Int): FirstModelArlanga {
    val p0 = fsth(n, lyambda, tObsl)
    val pk = fstk(k, lyambda, tObsl, p0)
    val potkaz = fstk(n, lyambda, tObsl, p0)
    val pobsl = 1 - potkaz
    return FirstModelArlanga(p0,pk, potkaz, pobsl, lyambda*pobsl, lyambda*tObsl*pobsl)
}

fun fsth(n: Int, lyambda: Double, tObsl: Double): Double {
    var rez = 0.0
    for (k: Int in 0..(n + 1)) {
        rez += ((lyambda * tObsl).pow(k.toDouble())) / calculateFactorial(k)
    }
    return rez.pow(-1)
}

fun fstk(k: Int, lyambda: Double, tObsl: Double, p0: Double): Double {
    return (((lyambda * tObsl).pow(k.toDouble())) / calculateFactorial(k)) * p0
}