package com.mikhno.myapplication.utils

import kotlin.math.pow

data class SecondModelArlanga(
    val p0: Double,
    val potkaz: Double,
    val pobsl: Double,
    val abs_prop_spos: Double,
    val avg_zan_canal: Double,
    val avg_in_que: Double,
    val avg_treb_in_system: Double,
    val w0: Double,
    val wc: Double,
    val m: Int
)

fun secondModelArlanga(n: Int, lyambda: Double, tObsl: Double, l: Int, p: Double) : SecondModelArlanga {
    val p0 = ssth(n, l, lyambda, tObsl)
    val potkaz = sstk(n, l, lyambda, tObsl, p0)
    val pobsl = 1 - potkaz
    val abs_prop_spos = lyambda * pobsl
    val avg_zan_canal = lyambda * tObsl * pobsl
    val avg_in_que = avgTrebInQue(n, l, lyambda, tObsl, p0)
    val avg_treb_in_system = avg_in_que + avg_zan_canal
    val w0 = avg_in_que/abs_prop_spos
    val wc = avg_treb_in_system/abs_prop_spos
    val m = findOptinalM(n, lyambda, tObsl, p0, p)
    return SecondModelArlanga(p0, potkaz, pobsl, abs_prop_spos, avg_zan_canal, avg_in_que, avg_treb_in_system, w0, wc, m)
}

fun ssth(n: Int, l: Int, lyambda: Double, tObsl: Double): Double {
    var rez = 0.0
    for (k in 0..(n + 1)) {
        rez += ((lyambda * tObsl).pow(k.toDouble())) / calculateFactorial(k)
    }
    rez += if (lyambda * tObsl == n.toDouble()) {
        ((lyambda * tObsl).pow(l.toDouble())) / calculateFactorial(n) * l
    } else {
        ((lyambda * tObsl).pow((n + 1).toDouble())) / calculateFactorial(n) * l / (n - lyambda * tObsl) * (1 - (lyambda * tObsl / n).pow(
            l
        ))
    }
    return rez.pow(-1)
}

fun sstk(k: Int, l: Int, lyambda: Double, tObsl: Double, p0: Double): Double {
    return (lyambda * tObsl).pow(k + 1) / calculateFactorial(k) / k.toDouble().pow(l) * p0
}

fun avgTrebInQue(n: Int, l: Int, lyambda: Double, tObsl: Double, p0: Double): Double {
    val r = lyambda * tObsl
    return when (n.toDouble() == r) {
        true -> r.pow(n) / calculateFactorial(n) * l * (l - 1) / 2 * p0
        false -> r.pow(n + 1) / calculateFactorial(n) / n * (1 + (l * r / n - l - 1) * (r / n).pow(l)) / (1 - r / n).pow(
            2
        ) * p0
    }
}

fun findOptinalM(n: Int, lyambda: Double, tObsl: Double, p0: Double, p: Double): Int {
    var m = 0
    while (1 - sstk(n, m, lyambda, tObsl, p0) < p) {
        m += 1
    }
    return m
}